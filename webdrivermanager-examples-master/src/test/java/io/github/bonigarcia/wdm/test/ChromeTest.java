/*
 * (C) Copyright 2016 Boni Garcia (http://bonigarcia.github.io/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.github.bonigarcia.wdm.test;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class ChromeTest {

    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setupTest() {
        driver = new ChromeDriver();
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void testProva() {
        WebDriverWait wait = new WebDriverWait(driver, 60);

        driver.get("https://www.sicredi.com.br/html/ferramenta/simulador-investimento-poupanca/");

        By selectInputEmpresa = By.xpath("//*[@id=\"formInvestimento\"]/div[1]/input[2]");
        wait.until(presenceOfElementLocated(selectInputEmpresa));
        driver.findElement(selectInputEmpresa).click();

        By texValorAplicar = By.id("valorAplicar");
        wait.until(presenceOfElementLocated(texValorAplicar));
        driver.findElement(texValorAplicar).sendKeys("10000");

        By textValorInvestir = By.id("valorInvestir");
        wait.until(presenceOfElementLocated(textValorInvestir));
        driver.findElement(textValorInvestir).sendKeys("5000");

        By tempoInput = By.id("tempo");
        wait.until(presenceOfElementLocated(tempoInput));
        driver.findElement(tempoInput).sendKeys("1");

        By btnAbrirOpcoes = By.xpath("//*[@id=\"formInvestimento\"]/div[4]/div[2]/div[2]/a/span[4]");
        wait.until(presenceOfElementLocated(btnAbrirOpcoes));
        driver.findElement(btnAbrirOpcoes).click();

        By btnSelectOpcaoAno = By.linkText("Anos");
        wait.until(elementToBeClickable(btnSelectOpcaoAno));
        driver.findElement(btnSelectOpcaoAno).click();

        By buttonSimular = By.cssSelector(".btnSimular");
        wait.until(elementToBeClickable(buttonSimular));
        driver.findElement(buttonSimular).click();

        By messageValor = By.cssSelector(".valor");
        wait.until(presenceOfElementLocated(messageValor));
        wait.until(textToBePresentInElementLocated(messageValor, "R$ 710"));
        assertThat(textToBePresentInElementLocated(messageValor, "R$ 710").toString(),
                containsString("R$ 710"));

        By buttonRefazer = By.cssSelector(".btnRefazer");
        wait.until(elementToBeClickable(buttonRefazer));
        driver.findElement(buttonRefazer).click();

        wait.until(presenceOfElementLocated(texValorAplicar));
        driver.findElement(texValorAplicar).sendKeys("1000");
        By mensagemErro = By.id("valorAplicar-error");
        assertThat(textToBePresentInElementLocated(mensagemErro, "Valor mínimo de 20.00").toString(),
                containsString("Valor mínimo de 20.00"));

        wait.until(presenceOfElementLocated(textValorInvestir));
        driver.findElement(textValorInvestir).sendKeys("200");
        By mensagemErro2 = By.id("valorInvestir-error");
        assertThat(textToBePresentInElementLocated(mensagemErro2, "Valor mínimo de 20.00").toString(),
                containsString("Valor mínimo de 20.00"));

        wait.until(elementToBeClickable(buttonSimular));
        driver.findElement(buttonSimular).click();
    }

}
