package br.com.dimed.projetoDimed.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class TransporteLocalizacaoID implements Serializable {

    @Column( name = "ID_TRANSPORTE" )
    private Integer idTransporte;

    @Column( name = "ID_LOCALIZACAO" )
    private Integer idLocalizacao;

    public TransporteLocalizacaoID( Integer idTransporte, Integer idLocalizacao) {
        this.idTransporte = idTransporte;
        this.idLocalizacao = idLocalizacao;
    }

}
