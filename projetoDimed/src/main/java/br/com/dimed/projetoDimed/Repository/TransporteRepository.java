package br.com.dimed.projetoDimed.Repository;

import br.com.dimed.projetoDimed.Entity.Transporte;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TransporteRepository extends CrudRepository<Transporte, Integer> {
    List<Transporte> findAll();

    Transporte findByCodigo(String codigo);
}