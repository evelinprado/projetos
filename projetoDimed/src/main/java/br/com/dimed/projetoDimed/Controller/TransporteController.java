package br.com.dimed.projetoDimed.Controller;


import br.com.dimed.projetoDimed.Entity.Transporte;
import br.com.dimed.projetoDimed.Service.TransporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/transportes")
public class TransporteController extends TransporteService {

    @Autowired
    TransporteService transporteService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Transporte> getTodos() {
        return transporteService.todosTransporte();
    }

    @GetMapping(value = "/implementacao")
    @ResponseBody
    public List<Transporte> implementacaoDosTransportes() {
        return transporteService.implementacao();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public List<Transporte> novo(@RequestBody Transporte transporte) {
        return transporteService.salvar(transporte);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Transporte editar(@PathVariable Integer id, @RequestBody Transporte transporte) {
        return transporteService.editar(id, transporte);
    }

    @GetMapping(value = "/deletar/{id}")
    @ResponseBody
    public void deletar(@PathVariable Integer id) {
        transporteService.deletar(id);
    }


}
