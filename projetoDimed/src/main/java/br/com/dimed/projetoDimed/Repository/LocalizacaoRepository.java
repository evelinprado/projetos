package br.com.dimed.projetoDimed.Repository;

import br.com.dimed.projetoDimed.Entity.Localizacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LocalizacaoRepository  extends CrudRepository<Localizacao, Integer> {

}