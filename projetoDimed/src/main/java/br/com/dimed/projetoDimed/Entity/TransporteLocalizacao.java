package br.com.dimed.projetoDimed.Entity;

import javax.persistence.*;

@Entity
public class TransporteLocalizacao {

    @EmbeddedId
    private TransporteLocalizacaoID id;

    private Integer idLinha;

    public TransporteLocalizacaoID getId() {
        return id;
    }

    public void setId(TransporteLocalizacaoID id) {
        this.id = id;
    }

    public Integer getIdLinha() {
        return idLinha;
    }

    public void setIdLinha(Integer idLinha) {
        this.idLinha = idLinha;
    }
}
