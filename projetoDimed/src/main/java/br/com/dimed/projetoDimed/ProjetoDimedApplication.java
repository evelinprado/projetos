package br.com.dimed.projetoDimed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoDimedApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoDimedApplication.class, args);
	}

}
