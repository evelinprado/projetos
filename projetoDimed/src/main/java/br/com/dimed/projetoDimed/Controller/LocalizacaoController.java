package br.com.dimed.projetoDimed.Controller;

import br.com.dimed.projetoDimed.Entity.Transporte;
import br.com.dimed.projetoDimed.Service.LocalizacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/api/localizacao")
public class LocalizacaoController extends LocalizacaoService {

    @Autowired
    LocalizacaoService localizacaoService;

    @PostMapping(value = "/localizacao")
    @ResponseBody
    public List<Transporte> localizarLinha(@RequestBody Long lat, @RequestBody Long lng, @RequestBody Integer raio) {
        return localizacaoService.localizandoTransportes(lat, lng, raio);
    }
}
