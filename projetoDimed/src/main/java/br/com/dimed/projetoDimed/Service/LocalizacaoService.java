package br.com.dimed.projetoDimed.Service;

import br.com.dimed.projetoDimed.Entity.Transporte;
import br.com.dimed.projetoDimed.Repository.LocalizacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocalizacaoService {

    @Autowired
    LocalizacaoRepository repository;

    public List<Transporte> localizandoTransportes(Long lat, Long lng, Integer raio){
        StringBuilder sql = new StringBuilder("SELECT *, (6371 *" +
                "acos(" +
                "cos(radians(" + lat + ")) *" +
                "cos(radians(lat)) *" +
                "cos(radians(" + lng + " - radians(lgn)) +" +
                "sin(radians(" + lat + ")) *" +
                "sin(radians(lat))" +
                ")) AS distance" +
                "FROM localizacao HAVING distance <= " + raio);
        return null;
    }
}
