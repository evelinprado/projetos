package br.com.dimed.projetoDimed.Repository;

import br.com.dimed.projetoDimed.Entity.TransporteLocalizacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransporteLocalizacaoRepository extends CrudRepository<TransporteLocalizacao, Integer> {
}
