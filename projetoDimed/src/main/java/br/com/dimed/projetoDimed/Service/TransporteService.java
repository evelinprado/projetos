package br.com.dimed.projetoDimed.Service;

import br.com.dimed.projetoDimed.Entity.Localizacao;
import br.com.dimed.projetoDimed.Entity.Transporte;
import br.com.dimed.projetoDimed.Entity.TransporteLocalizacao;
import br.com.dimed.projetoDimed.Entity.TransporteLocalizacaoID;
import br.com.dimed.projetoDimed.Repository.LocalizacaoRepository;
import br.com.dimed.projetoDimed.Repository.TransporteLocalizacaoRepository;
import br.com.dimed.projetoDimed.Repository.TransporteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class TransporteService {

    @Autowired
    TransporteRepository repository;

    @Autowired
    LocalizacaoRepository localizacaoRepository;

    @Autowired
    TransporteLocalizacaoRepository repositoryTL;

    @Autowired
    RestTemplate restTemplate;

    public List<Transporte> todosTransporte() {
        return repository.findAll();
    }

    public List<Transporte> salvar(Transporte transporte) {
        Transporte result = repository.findByCodigo(transporte.getCodigo());
        if(result == null) {
            repository.save(transporte);
        } else {
            editar(result.getId(), transporte);
        }
        return repository.findAll();
    }

    public Transporte editar(Integer id, Transporte transporte) {
        transporte.setId(id);
        return repository.save(transporte);
    }

    public void deletar(Integer id) {
        repository.deleteById(id);
    }

    public List<Transporte> implementacao() {
        Localizacao cordenada = new Localizacao();
        TransporteLocalizacao transporteLocalizacao = new TransporteLocalizacao();
        Transporte[] forTransp = restTemplate.
                getForObject("http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o",
                        Transporte[].class);
        for (Transporte transporte : forTransp){
            repository.save(transporte);
            Localizacao[] forLocalizacao = restTemplate.
                    getForObject("http://www.poatransporte.com.br/php/facades/process.php?a=il&p="+ transporte.getId(),
                            Localizacao[].class);
            for (Localizacao localizacao : forLocalizacao) {
                cordenada.setLat(localizacao.getLat());
                cordenada.setLng(localizacao.getLng());
                cordenada.setIdLocalizacao(localizacao.getIdLocalizacao());
                localizacaoRepository.save(cordenada);
                transporteLocalizacao.setId(new TransporteLocalizacaoID(transporte.getId(),cordenada.getIdLocalizacao()));
                transporteLocalizacao.setIdLinha(transporte.getId());
                repositoryTL.save(transporteLocalizacao);
            }
        }
        return Arrays.asList(forTransp);
    }
}
