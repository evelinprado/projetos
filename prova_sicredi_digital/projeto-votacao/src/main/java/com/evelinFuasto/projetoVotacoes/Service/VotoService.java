package com.evelinFuasto.projetoVotacoes.Service;

import com.evelinFuasto.projetoVotacoes.Entity.Pauta;
import com.evelinFuasto.projetoVotacoes.Entity.RegistroVoto;
import com.evelinFuasto.projetoVotacoes.Entity.Voto;
import com.evelinFuasto.projetoVotacoes.MB.VotoMB;
import com.evelinFuasto.projetoVotacoes.Repository.RegistroVotoRepository;
import com.evelinFuasto.projetoVotacoes.Repository.VotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class VotoService {

    @Autowired
    VotoRepository repository;

    @Autowired
    RegistroVotoRepository registroVotoRepository;

    public Voto salvarVoto(Long idOpcaoVoto, Voto voto) {
        Voto votoAssociado = new Voto();
        if (repository.countByIdAssociadoAndPauta(voto.getIdAssociado(), voto.getPauta()) == 0) {
            Pauta pauta = repository.findByPauta(voto.getPauta());
            if (pauta != null) {
                if (verificaPodeVotar(pauta)) {
                    votoAssociado.setIdAssociado(voto.getIdAssociado());
                    votoAssociado.setPauta(pauta);
                    repository.save(votoAssociado);
                    registrarVoto(idOpcaoVoto, pauta);
                    return votoAssociado;
                }
            }
        }
        return null;
    }

    private boolean verificaPodeVotar(Pauta pauta) {
        Date dataAtual = new Date();
        if (pauta.getDataInicio().before(dataAtual) && pauta.getDataFim().after(dataAtual)) {
            return true;
        }
        return false;
    }

    private void registrarVoto(Long idOpcaoVoto, Pauta pauta ) {
        RegistroVoto registroVoto = registroVotoRepository.findByIdOpcaoVotoAndAndIdPauta(idOpcaoVoto, pauta.getIdPauta());
        registroVoto. setQtdVotos(registroVoto.getQtdVotos() + 1);
        registroVotoRepository.save(registroVoto);
    }

}
