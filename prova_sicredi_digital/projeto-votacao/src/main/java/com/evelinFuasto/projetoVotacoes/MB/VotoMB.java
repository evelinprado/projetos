package com.evelinFuasto.projetoVotacoes.MB;

public class VotoMB {

    private Long idPauta;
    private Long idAssociado;
    private Long idOpcaoVoto;

    public VotoMB(Long idPauta, Long idAssociado, Long idOpcaoVoto) {
        this.idPauta = idPauta;
        this.idAssociado = idAssociado;
        this.idOpcaoVoto = idOpcaoVoto;
    }

    public Long getIdPauta() {
        return idPauta;
    }

    public void setIdPauta(Long idPauta) {
        this.idPauta = idPauta;
    }

    public Long getIdAssociado() {
        return idAssociado;
    }

    public void setIdAssociado(Long idAssociado) {
        this.idAssociado = idAssociado;
    }

    public Long getIdOpcaoVoto() {
        return idOpcaoVoto;
    }

    public void setIdOpcaoVoto(Long idOpcaoVoto) {
        this.idOpcaoVoto = idOpcaoVoto;
    }
}
