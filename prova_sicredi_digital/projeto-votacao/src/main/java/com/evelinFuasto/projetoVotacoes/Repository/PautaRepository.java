package com.evelinFuasto.projetoVotacoes.Repository;

import com.evelinFuasto.projetoVotacoes.Entity.Pauta;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PautaRepository extends CrudRepository<Pauta, Long> {
    List<Pauta> findAll();

    Pauta findByIdPauta(Long idPauta);
}
