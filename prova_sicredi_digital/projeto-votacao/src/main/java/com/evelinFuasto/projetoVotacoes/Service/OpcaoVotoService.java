package com.evelinFuasto.projetoVotacoes.Service;

import com.evelinFuasto.projetoVotacoes.Entity.OpcaoVoto;
import com.evelinFuasto.projetoVotacoes.MB.VotoMB;
import com.evelinFuasto.projetoVotacoes.Repository.OpcaoVotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OpcaoVotoService {

    @Autowired
    OpcaoVotoRepository repository;

    public void salvarVoto(VotoMB voto) {
//        OpcaoVoto votacao = new OpcaoVoto();
//        votacao = repository.findByIdOpcaoVotoAndPauta(voto.getIdPauta(), voto.getVoto());
//        repository.save(votacao);
    }

    public void salvar(OpcaoVoto opcaoVoto) {
        repository.save(opcaoVoto);
    }

}
