package com.evelinFuasto.projetoVotacoes.Service;

import com.evelinFuasto.projetoVotacoes.Entity.Pauta;
import com.evelinFuasto.projetoVotacoes.Entity.RegistroVoto;
import com.evelinFuasto.projetoVotacoes.MB.PautaMB;
import com.evelinFuasto.projetoVotacoes.Repository.PautaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PautaService {

    @Autowired
    PautaRepository repository;

    @Autowired
    RegistroVotoService registroVotoService;

    public void cadastroPauta(Pauta pauta) {
        repository.save(pauta);
    }

    public boolean iniciarVotacao(Long id, int segundos) {
        Pauta pauta = repository.findByIdPauta(id);
        if(pauta.getDataInicio() == null) {
            Date data = new Date();
            pauta.setDataInicio(data);
            if (segundos != 0) {
                data.setSeconds(data.getSeconds() + segundos);
            } else {
                data.setSeconds(data.getSeconds() + 60);
            }
            pauta.setDataFim(data);
            repository.save(pauta);
            registroVotoService.iniciarRegistro(id);
            return true;
        }
        return false;
    }

    public PautaMB contabilizarVotacao(Long idPauta) {
        Pauta pauta = repository.findByIdPauta(idPauta);
        PautaMB pautaFinalizada = new PautaMB();
        pautaFinalizada.setIdPauta(pauta.getIdPauta());
        pautaFinalizada.setDescricaoPauta(pauta.getDescricao());
        pautaFinalizada.setListaParticipantes(pauta.getVoto());
        pautaFinalizada.setTotalVotos(totalVotos(idPauta));
        return pautaFinalizada;
    }

    public Pauta buscaPauta(Long id) {
        return repository.findByIdPauta(id);
    }

    private int totalVotos(Long idPauta) {
        List<RegistroVoto> votos = registroVotoService.todosRegistros(idPauta);
        List<Integer> somaVotos = votos.stream().map(vt -> vt.getQtdVotos()).collect(Collectors.toList());
        int somaTotal = 0;
        for (int total : somaVotos) {
            somaTotal =+ total;
        }
        return somaTotal;
    }
}
