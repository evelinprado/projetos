package com.evelinFuasto.projetoVotacoes.ApiV1.Controller;

import com.evelinFuasto.projetoVotacoes.ApiV1.Response.CpfStatus;
import com.evelinFuasto.projetoVotacoes.Entity.Voto;
import com.evelinFuasto.projetoVotacoes.Service.VotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Controller
@RequestMapping("/v1/voto")
public class VotoController {

    @Autowired
    VotoService votoService;

    @PutMapping(value = "/{idOpcaoVoto}")
    public ResponseEntity cadastraVoto(@PathVariable Long idOpcaoVoto, @RequestBody Voto voto) {
        if (voto != null && verificaCpf(voto.getIdAssociado())) {
            Voto retorno = votoService.salvarVoto(idOpcaoVoto, voto);
            return ResponseEntity.ok(retorno);
        }
        return null;
    }

    private boolean verificaCpf(Long cpf) {
        RestTemplate restTemplate = new RestTemplate();
        URI url = URI.create("https://user-info.herokuapp.com/users/".concat(String.valueOf(cpf)));
        CpfStatus cpfStatus = restTemplate.getForObject(url, CpfStatus.class);
        if (cpfStatus.getStatus().equals("UNABLE_TO_VOTE")) {
            return false;
        }
        if (cpfStatus.getStatus().equals("ABLE_TO_VOTE")) {
            return true;
        }
        return false;
    }
}
