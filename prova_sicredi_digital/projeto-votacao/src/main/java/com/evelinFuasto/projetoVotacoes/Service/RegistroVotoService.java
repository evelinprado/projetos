package com.evelinFuasto.projetoVotacoes.Service;

import com.evelinFuasto.projetoVotacoes.Entity.RegistroVoto;
import com.evelinFuasto.projetoVotacoes.Repository.RegistroVotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegistroVotoService {

    @Autowired
    RegistroVotoRepository repository;

    public void iniciarRegistro(Long idPauta) {
        RegistroVoto registroSim = new RegistroVoto(1L, idPauta, 0);
        RegistroVoto registroNao = new RegistroVoto(2L, idPauta, 0);
        repository.save(registroSim);
        repository.save(registroNao);
    }

    public List<RegistroVoto> todosRegistros( Long idPauta) {
        return repository.findByIdPauta(idPauta);
    }
}
