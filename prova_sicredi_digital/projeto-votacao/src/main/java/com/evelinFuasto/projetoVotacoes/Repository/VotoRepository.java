package com.evelinFuasto.projetoVotacoes.Repository;

import com.evelinFuasto.projetoVotacoes.Entity.Pauta;
import com.evelinFuasto.projetoVotacoes.Entity.Voto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VotoRepository extends CrudRepository<Voto, Long> {
    List<Voto> findAll();
    int countByIdAssociadoAndPauta(Long idAssociado, Pauta pauta);
    Pauta findByPauta(Pauta pauta);
}
