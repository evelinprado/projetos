package com.evelinFuasto.projetoVotacoes;

import com.evelinFuasto.projetoVotacoes.Entity.OpcaoVoto;
import com.evelinFuasto.projetoVotacoes.Service.OpcaoVotoService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public CommandLineRunner hml(OpcaoVotoService opcaoVotoService) {
        OpcaoVoto opcaoSim = new OpcaoVoto();
        opcaoSim.setDescrição("SIM");
        OpcaoVoto opcaoNao = new OpcaoVoto();
        opcaoNao.setDescrição("NÃO");

        return args -> {
            opcaoVotoService.salvar(opcaoSim);
            opcaoVotoService.salvar(opcaoNao);
        };
    }

}