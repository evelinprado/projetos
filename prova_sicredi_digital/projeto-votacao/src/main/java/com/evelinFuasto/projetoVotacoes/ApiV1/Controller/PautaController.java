package com.evelinFuasto.projetoVotacoes.ApiV1.Controller;

import com.evelinFuasto.projetoVotacoes.Entity.Pauta;
import com.evelinFuasto.projetoVotacoes.Service.PautaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping("/v1/pauta")
public class PautaController {

    @Autowired
    PautaService service;


    @PostMapping(value = "/cadastroPauta")
    @ResponseBody
    public ResponseEntity cadatrarPauta(@Valid @RequestBody() Pauta pauta) {
        if (pauta != null) {
            pauta.setDataCadastro(new Date());
            service.cadastroPauta(pauta);
        }
        return ResponseEntity.ok(pauta);
    }

    @GetMapping(value = "/iniciarVotacao/{id}/tempo/{segundos}")
    public ResponseEntity iniciarVotacao(@PathVariable Long id, @PathVariable int segundos) throws Exception {
        if (id != null ) {
            if( service.iniciarVotacao(id, segundos)) {
                return ResponseEntity.ok(id);
            }
        }
        return ResponseEntity.ok(null);
    }

    @GetMapping(value = "/contabilizar/{idPauta}")
    public ResponseEntity contabilizarVotacao(@PathVariable Long idPauta) {
        if (idPauta != null) {
            service.contabilizarVotacao(idPauta);
        }
        return ResponseEntity.ok(idPauta);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity buscaPauta(@PathVariable Long id) throws Exception {
        if (id != null) {
            Pauta pauta = service.buscaPauta(id);
            return ResponseEntity.ok(pauta);
        }
        return ResponseEntity.ok(null);
    }
}
