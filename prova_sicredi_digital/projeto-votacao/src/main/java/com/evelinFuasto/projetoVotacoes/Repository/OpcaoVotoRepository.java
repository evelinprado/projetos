package com.evelinFuasto.projetoVotacoes.Repository;

import com.evelinFuasto.projetoVotacoes.Entity.OpcaoVoto;
import com.evelinFuasto.projetoVotacoes.Enum.TipoOpcaoVoto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OpcaoVotoRepository extends CrudRepository<OpcaoVoto, Long> {
    List<OpcaoVoto> findAll();
}
