package com.evelinFuasto.projetoVotacoes.Entity;

import com.evelinFuasto.projetoVotacoes.Enum.TipoOpcaoVoto;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;


@Entity
@Table(name = "OPCAO_VOTO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idOpcaoVoto", scope = OpcaoVoto.class)
public class OpcaoVoto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_OPCAO_VOTO")
    private Long idOpcaoVoto;

    @Column
    private String descrição;

    public OpcaoVoto() {
    }

    public Long getIdOpcaoVoto() {
        return idOpcaoVoto;
    }

    public void setIdOpcaoVoto(Long idOpcaoVoto) {
        this.idOpcaoVoto = idOpcaoVoto;
    }

    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }
}
