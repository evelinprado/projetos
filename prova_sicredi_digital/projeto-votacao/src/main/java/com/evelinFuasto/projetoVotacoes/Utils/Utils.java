package com.evelinFuasto.projetoVotacoes.Utils;

import java.math.BigDecimal;

public class Utils {

    private Integer convertObjectBigDecimalToInteger(Object bigDecimal) {
        return new BigDecimal(bigDecimal.toString()).intValue();
    }

}
