package com.evelinFuasto.projetoVotacoes.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.*;

@Entity
@Table(name = "VOTO")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "idVoto", scope = Voto.class)
public class Voto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_VOTO")
    private Long idVoto;

    @JoinColumn(name = "ID_PAUTA", referencedColumnName = "ID_PAUTA", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pauta pauta;

    @Column
    private Long idAssociado;

    public Long getIdVoto() {
        return idVoto;
    }

    public void setIdVoto(Long idVoto) {
        this.idVoto = idVoto;
    }

    public Long getIdAssociado() {
        return idAssociado;
    }

    public void setIdAssociado(Long idAssociado) {
        this.idAssociado = idAssociado;
    }

    public Pauta getPauta() {
        return pauta;
    }

    public void setPauta(Pauta pauta) {
        this.pauta = pauta;
    }
}
