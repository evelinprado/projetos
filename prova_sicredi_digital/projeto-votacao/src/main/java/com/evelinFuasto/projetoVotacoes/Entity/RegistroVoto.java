package com.evelinFuasto.projetoVotacoes.Entity;

import javax.persistence.*;

@Entity
public class RegistroVoto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_REGISTRO_VOTO")
    private Long idRegistroVoto;

    @Column
    private Long idOpcaoVoto;

    @Column
    private long idPauta;

    @Column
    private int qtdVotos;

    public RegistroVoto(Long idOpcaoVoto, long idPauta, int qtdVotos) {
        this.idOpcaoVoto = idOpcaoVoto;
        this.idPauta = idPauta;
        this.qtdVotos = qtdVotos;
    }

    public Long getIdRegistroVoto() {
        return idRegistroVoto;
    }

    public void setIdRegistroVoto(Long idRegistroVoto) {
        this.idRegistroVoto = idRegistroVoto;
    }

    public Long getIdOpcaoVoto() {
        return idOpcaoVoto;
    }

    public void setIdOpcaoVoto(Long idOpcaoVoto) {
        this.idOpcaoVoto = idOpcaoVoto;
    }

    public long getIdPauta() {
        return idPauta;
    }

    public void setIdPauta(long idPauta) {
        this.idPauta = idPauta;
    }

    public int getQtdVotos() {
        return qtdVotos;
    }

    public void setQtdVotos(int qtdVotos) {
        this.qtdVotos = qtdVotos;
    }
}
