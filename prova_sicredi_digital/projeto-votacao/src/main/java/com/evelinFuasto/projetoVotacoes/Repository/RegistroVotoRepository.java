package com.evelinFuasto.projetoVotacoes.Repository;


import com.evelinFuasto.projetoVotacoes.Entity.RegistroVoto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RegistroVotoRepository extends CrudRepository<RegistroVoto, Long> {
    List<RegistroVoto> findAll();
    RegistroVoto findByIdOpcaoVotoAndAndIdPauta(Long idOpcaoVoto, Long idPauta);
    List<RegistroVoto> findByIdPauta(Long idPauta);
}
