package com.evelinFuasto.projetoVotacoes.MB;

import com.evelinFuasto.projetoVotacoes.Entity.OpcaoVoto;
import com.evelinFuasto.projetoVotacoes.Entity.Pauta;
import com.evelinFuasto.projetoVotacoes.Entity.Voto;

import java.util.List;

public class PautaMB {

    private Long idPauta;
    private String descricaoPauta;
    private List<OpcaoVoto> opcaoVotos;
    private int totalVotos;
    private List<Voto> listaParticipantes;

    public PautaMB() {

    }

    public PautaMB(List<Voto> listaParticipantes) {
        this.listaParticipantes = listaParticipantes;
    }

    public PautaMB(Long idPauta, String descricaoPauta, List<OpcaoVoto> votos, int totalVotos, List<Voto> listaParticipantes) {
        this.idPauta = idPauta;
        this.descricaoPauta = descricaoPauta;
        this.opcaoVotos = votos;
        this.totalVotos = totalVotos;
        this.listaParticipantes = listaParticipantes;
    }

    public Long getIdPauta() {
        return idPauta;
    }

    public void setIdPauta(Long idPauta) {
        this.idPauta = idPauta;
    }

    public String getDescricaoPauta() {
        return descricaoPauta;
    }

    public void setDescricaoPauta(String descricaoPauta) {
        this.descricaoPauta = descricaoPauta;
    }

    public List<OpcaoVoto> getVotos() {
        return opcaoVotos;
    }

    public void setVotos(List<OpcaoVoto> opcaoVotos) {
        this.opcaoVotos = opcaoVotos;
    }

    public int getTotalVotos() {
        return totalVotos;
    }

    public void setTotalVotos(int totalVotos) {
        this.totalVotos = totalVotos;
    }

    public List<Voto> getListaParticipantes() {
        return listaParticipantes;
    }

    public void setListaParticipantes(List<Voto> listaParticipantes) {
        this.listaParticipantes = listaParticipantes;
    }
}
